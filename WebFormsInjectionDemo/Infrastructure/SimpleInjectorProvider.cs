﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace WebFormsInjectionDemo.Infrastructure
{
    public class SimpleInjectorProvider : IServiceProvider
    {
        public object GetService(Type serviceType)
        {
            if (IsRegisteredInContainer(serviceType))
            {
                return ResolveFromContainer(serviceType);
            }
            else
            {
                return CreateByReflection(serviceType);
            }
        }


        private bool IsRegisteredInContainer(Type serviceType)
        {
            return Global.Container.GetRegistration(serviceType) != null;
        }
        private object ResolveFromContainer(Type serviceType)
        {
            return Global.Container.GetInstance(serviceType);
        }
        private static object CreateByReflection(Type serviceType)
        {
            return Activator.CreateInstance(
                serviceType,
                BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.CreateInstance,
                null,
                null,
                null);
        }

    }
}
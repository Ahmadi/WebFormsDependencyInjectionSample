﻿using System.Collections.Generic;
using System.Linq;
using FizzWare.NBuilder;

namespace WebFormsInjectionDemo.Customers
{
    public class CustomerService : ICustomerService
    {
        public List<Customer> LoadAll()
        {
            return Builder<Customer>.CreateListOfSize(10)
                .All()
                .With(a => a.Firstname = Faker.NameFaker.FirstName())
                .With(a => a.Lastname = Faker.NameFaker.LastName())
                .Build()
                .ToList();
        }
    }
}
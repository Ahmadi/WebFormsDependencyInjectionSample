﻿using System.Collections.Generic;

namespace WebFormsInjectionDemo.Customers
{
    public interface ICustomerService
    {
        List<Customer> LoadAll();
    }
}
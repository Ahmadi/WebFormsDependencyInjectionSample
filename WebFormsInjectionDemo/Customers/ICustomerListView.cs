﻿using System.Collections.Generic;

namespace WebFormsInjectionDemo.Customers
{
    public interface ICustomerListView
    {
        void ShowCustomers(List<Customer> customers);
    }
}
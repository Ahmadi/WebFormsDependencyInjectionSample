﻿namespace WebFormsInjectionDemo.Customers
{
    public class CustomerListPresenter : ICustomerListPresenter
    {
        private readonly ICustomerService _service;
        public CustomerListPresenter(ICustomerService service)
        {
            _service = service;
        }

        public void Initial(ICustomerListView view)
        {
            var customers = _service.LoadAll();
            view.ShowCustomers(customers);
        }
    }
}
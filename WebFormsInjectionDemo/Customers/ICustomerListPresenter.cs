﻿namespace WebFormsInjectionDemo.Customers
{
    public interface ICustomerListPresenter
    {
        void Initial(ICustomerListView view);
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WebFormsInjectionDemo.Customers;

namespace WebFormsInjectionDemo
{
    public partial class CustomerList : System.Web.UI.Page, ICustomerListView
    {
        private readonly ICustomerListPresenter _presenter;
        public CustomerList(ICustomerListPresenter presenter)
        {
            this._presenter = presenter;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            _presenter.Initial(this);
        }

        public void ShowCustomers(List<Customer> customers)
        {
            gridViewCustomers.DataSource = customers;
            gridViewCustomers.DataBind();
        }
    }
}
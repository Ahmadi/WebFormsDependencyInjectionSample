﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using SimpleInjector;
using WebFormsInjectionDemo.Customers;
using WebFormsInjectionDemo.Infrastructure;

namespace WebFormsInjectionDemo
{
    public class Global : System.Web.HttpApplication
    {
        public static Container Container { get; private set; }
        protected void Application_Start(object sender, EventArgs e)
        {
            SetupContainer();
        }
        public override void Init()
        {
            HttpRuntime.WebObjectActivator = new SimpleInjectorProvider();
            base.Init();
        }
        private static void SetupContainer()
        {
            Container = new Container();
            Container.Register<ICustomerListPresenter, CustomerListPresenter>(Lifestyle.Transient);
            Container.Register<ICustomerService, CustomerService>(Lifestyle.Transient);
            Container.Verify();
        }
    }
}